# Bài tập lập trình toán học

## Các phép toán đơn giản

### 1. Tổng hai số
Cho hai số bất kì, tính tổng của chúng.
> inp: 6 9

> out: 15

### 2. Diện tích
Cho một mảnh đất hình vuông có độ dài một cạnh là **n**, hãy tính diện tích 
mảnh đất.
> inp: 5

> out: 25

### 3. Căn bậc hai
Cho **n**, tính căn bậc hai của **n**.
> inp: 9

> out: 3

### 4. Kiểm tra ước
Số **a** được gọi là ước của số **b** nếu **b** chia hết cho **a**. Cho hai số 
bất kì, in ra *True* nếu một trong hai số là ước của số còn lại, ngược lại 
in ra *False*.

> inp: 27 9

> out: True
---
> inp: 9 30

> out: False

### 5. Tam giác cân
Cho số đo 3 cạnh của một tam giác là **a**, **b** ,**c**. Hãy viết chương trình kiểm tra xem 3 cạnh đó có phải là 3 cạnh của một tam giác cân hay không và tính chu vi của tam giác đó.

> inp: 5 5 3

> out: la tam giac can, chu vi = 13

---

> inp: 4 5 3

> out: khong phai la tam giac can

### 6. Lãi suất
Bạn gửi ngân hàng một số tiền (dollar) với lãi suất 6%/năm, tính số tiền lãi 
bạn nhận được sau **n** năm.

> inp: 1000 3

> out: 180

_(Giải thích: 1000 là số tiền gửi, 3 là số năm)_

## Các bài toán cơ bản

### 7. Bảng nhân
Nhập vào một số **n**, in ra bảng nhân của n từ 1 đến 10.

> inp: 5

> out:

* 5 x 1 = 5

* 5 x 2 = 10

* ...

* 5 x 10 = 50

### 8. Ước chung - Bội chung
Cho 2 số bất kì, tìm ước chung lớn nhất và bội chung nhỏ nhất

> inp: 45 75

> out: ucln = 15, bcnn = 225

### 9. Fibonacci 
Dãy fibonacci là dãy tăng dần theo tính chất phần tử này bằng tổng của hai 
phần tử liền trước nó. Hãy viết chương trình in ra dãy fibonacci từ phần tử 
1 đến phần tử n. Biết phần tử 1 và 2 đều có giá trị bằng 1

> inp: 6

> out: 1 1 2 3 5 8